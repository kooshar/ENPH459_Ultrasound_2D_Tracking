#!/usr/bin/env python

# This sample shows how to take advantage of wx.CallAfter when running a
# separate thread and updating the GUI in the main thread
import calibration as cal
import wx
import threading
import gui
import time
import cv2
import io
import serial

#constant
camNum = 1
numFrame = 5
LENGTH_SCALE=10
minknn=2
minbff=2
posaccuracy = 0.2

#IMU
COMport ='COM4'
baudRate = 9600
timeout = 0.02


class MainFrame(wx.Frame):

    def __init__(self, parent):
        #shared constants
        self.mtx = 0
        self.dist = 0
        self.camNum = camNum
        self.frame_num = numFrame
        self.lengthScale = LENGTH_SCALE
        self.minknn = minknn
        self.minBff = minbff
        self.posAccuracy = posaccuracy
                
        wx.Frame.__init__(self, parent, title='Tracking',size=(900,600),style=wx.SYSTEM_MENU | wx.CAPTION |wx.RESIZE_BORDER)
        self.Center()
         
        #ser = serial.Serial(COMport, baudrate, timeout) 
        #self.sio = io.TextIOWrapper(io.BufferedRWPair(ser, ser))
        
        self.lennaArray = [.0,.0,.0]
        self.lennaLock = threading.Lock()        
        self.panel = wx.Panel(self)
        
        self.camera = wx.ComboBox(self.panel, -1, size=(150, 20), choices=['Camera: #0','Camera: #1','Camera: #2','Camera: #3','Camera: #4'], style=wx.CB_READONLY)
        self.camera.SetSelection(0)
        self.camera.Bind(wx.EVT_COMBOBOX ,self.OnSelect)
        
        self.btn = wx.Button(self.panel, label="Calibrate")
        self.lennabtn = wx.Button(self.panel, label="load scale")
        self.lennabtn.Enable(False)
        self.Quit = wx.Button(self.panel, label="Quit")
        self.gauge = wx.Gauge(self.panel)
        
        self.vidPlayer = wx.StaticBitmap(self.panel)
        self.trackPlayer = wx.StaticBitmap(self.panel,size=(300,300))
        
        self.Bind(wx.EVT_BUTTON, self.OnButton)
        self.Bind(wx.EVT_SIZE, self.OnSize)
               
        self.webcam = cv2.VideoCapture()
        self.webcam.release()
        
        self.makeMenuBar()
        self.CreateStatusBar()
        self.SetStatusText("Hi! Please calibrate before start!")
    
    def OnSelect(self, event):
        self.camNum=event.GetSelection();
        
    def OnSize(self, event):
        FWidth=self.ClientSize[0]
        FHeight=self.ClientSize[1]
        
        self.panel.SetSize(0, 0, FWidth, FHeight)
        self.camera.SetPosition((3*self.btn.GetSize()[0]+20,FHeight-self.gauge.GetSize()[1]-self.btn.GetSize()[1]-8))
        self.btn.SetPosition((self.btn.GetSize()[0],FHeight-self.gauge.GetSize()[1]-self.btn.GetSize()[1]-10))
        self.lennabtn.SetPosition((2*self.btn.GetSize()[0]+10,FHeight-self.gauge.GetSize()[1]-self.btn.GetSize()[1]-10))
        self.Quit.SetPosition((FWidth-2*self.Quit.GetSize()[0],FHeight-self.gauge.GetSize()[1]-self.Quit.GetSize()[1]-10))
        self.gauge.SetPosition((0,FHeight-self.gauge.GetSize()[1]))
        self.gauge.SetSize(0, FHeight-self.gauge.GetSize()[1], FWidth, self.gauge.GetSize()[1])
        self.trackPlayer.SetPosition(pt=(self.ClientSize[0]-self.trackPlayer.Size[0],0))
        
        self.panel.Refresh()
        
    def makeMenuBar(self):
        fileMenu = wx.Menu()
        loadItem = fileMenu.Append(-1, "&load...\tCtrl-l","load calibration")
        fileMenu.AppendSeparator()
        exitItem = fileMenu.Append(wx.ID_EXIT)
        helpMenu = wx.Menu()
        aboutItem = helpMenu.Append(wx.ID_ABOUT)
        
        menuBar = wx.MenuBar()
        menuBar.Append(fileMenu, "&File")
        menuBar.Append(helpMenu, "&Help")

        self.SetMenuBar(menuBar)
        self.Bind(wx.EVT_MENU, self.OnLoad, loadItem)
        self.Bind(wx.EVT_MENU, self.OnExit,  exitItem)
        self.Bind(wx.EVT_MENU, self.OnAbout, aboutItem)
          
        
    def OnButton(self, event):
        btn = event.GetEventObject()
        threading.Thread(target=self.ButtonAction, args=(btn,)).start()  
           
    def OnExit(self, event):
        self.Close(True)

    def OnLoad(self, event):
        ret, self.mtx, self.dist = cal.findCamMTX('cal_im\\*.jpg')
        if (ret):
            wx.MessageBox("Calibration loaded")
            self.SetStatusText("Calibrated! Ready to start!")
            self.btn.SetLabel('Initialize')
        else:
            wx.MessageBox("Calibration Failed")
            self.SetStatusText("NOT Calibrated! Please try to calibrate again before start!")
        
    def OnAbout(self, event):
        wx.MessageBox("ENPH 459", "About Tracking",wx.OK|wx.ICON_INFORMATION)
    
    def ButtonAction(self, btn):
        if(btn.GetLabel()=='Quit'):
            self.btn.SetLabel('Quitting')
            while(self.webcam.isOpened()):
                time.sleep(0.2)
            while(hasattr(self,'LennaThread') and  self.LennaThread.isAlive()):
                time.sleep(0.1)
            cv2.destroyAllWindows()
            self.Close(True)
            
        elif(btn.GetLabel()=='load scale'):
            self.scaleThread = threading.Thread(target=gui.scalePic, args=(self,))
            self.scaleThread.start()
            self.lennabtn.SetLabel('calc. scale')
            
        elif(btn.GetLabel()=='calc. scale'):
            self.lennabtn.SetLabel('calculating')
            while self.scaleThread.isAlive():
                time.sleep(0.1)
            if (hasattr(self,'LennaThread') and  self.LennaThread.isAlive()):
                self.lennabtn.SetLabel('load scale')
            else:
                self.lennabtn.SetLabel('load scale')
                self.LennaThread = threading.Thread(target=gui.lenna, args=(self,))
                self.LennaThread.start()
            self.lennabtn.Enable(True)  
            
        elif(btn.GetLabel()=='Calibrate'):
            ret, self.mtx, self.dist = cal.findCamMTX('cal_im\\*.jpg')
            if (ret):
                self.SetStatusText("Calibrated! Ready to start!")
                self.btn.SetLabel('Initialize')
            else:
                self.SetStatusText("NOT Calibrated! Please try to calibrate again before start!")
            
        elif(btn.GetLabel()=='Initialize'):
            self.lennabtn.Enable(True)  
            self.gauge.SetValue(70)
            #set the btn status/label
            self.SetStatusText("Ready to start!")
            self.btn.SetLabel('Start Tracking') 
            
            #make sure other threads using the camera are done and start initializing
            while(self.webcam.isOpened()):
                time.sleep(0.1)

            self.InitializeThread = threading.Thread(target=gui.Initialize,args=(self,))
            self.InitializeThread.start()
                   
        elif(btn.GetLabel()=='Start Tracking'):
            #set the btn status/label
            self.btn.SetLabel('Done')  
            self.SetStatusText("Tracking...")
            #make sure other threads using the camera are done and start tracking
            while(self.webcam.isOpened()):
                time.sleep(0.1)
            self.TargetThread = threading.Thread(target=gui.Track,args=(self,))
            self.TargetThread.start()
                
        elif(btn.GetLabel()=='Done'):
            self.SetStatusText("Done")
            self.btn.SetLabel('Initialize')
            self.btn.Enable(True)
        

if __name__ == "__main__":
    app = wx.App(0)
    frame = MainFrame(None)
    frame.Show()
    app.MainLoop()
