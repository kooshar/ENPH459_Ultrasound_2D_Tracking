import numpy as np
import cv2
import glob
import math

def findscale(MainFrame, squareImgLength):
    mtx=MainFrame.mtx
    dist=MainFrame.dist
    
    if(MainFrame.webcam.isOpened()):                  
        ret, frame = MainFrame.webcam.read()
        f = undistFrame(frame, mtx, dist)
        gray = cv2.cvtColor(f, cv2.COLOR_BGR2GRAY)
        
        try:
            ret,gray = cv2.threshold(gray,127,255,0)
            edges = cv2.Canny(gray,100,200,apertureSize = 3)
            
            lines = cv2.HoughLines(edges,1,np.pi/180,200)
            
            theta2=-1000
            rho2max=0
            rho2min=0
            rho1,theta1 = lines[0,0]
            rho1max=rho1
            rho1min=rho1
            for i in range(0,lines.size/2):
                for rho,theta in lines[i]:
                    if theta<theta1*1.15 and theta>theta1*0.85:
                        if rho>rho1max:
                            rho1max=rho
                        if rho<rho1min:
                            rho1min=rho
                    elif theta2==-1000:
                        theta2=theta
                        rho2max=rho
                        rho2min=rho
                    elif theta<theta2*1.15 and theta>theta2*0.85:
                        if rho>rho2max:
                            rho2max=rho
                        if rho<rho2min:
                            rho2min=rho
            
            diff1=abs(rho1max-rho1min)
            diff2=abs(rho2max-rho2min)
            
            diff= max(diff1,diff2)
            scale =diff/20
            
            for i in range(0,lines.size/2):
                for rho,theta in lines[i]:
                    a = np.cos(theta)
                    b = np.sin(theta)
                    x0 = a*rho
                    y0 = b*rho
                    x1 = int(x0 + 1000*(-b))
                    y1 = int(y0 + 1000*(a))
                    x2 = int(x0 - 1000*(-b))
                    y2 = int(y0 - 1000*(a))
            
                cv2.line(f,(x1,y1),(x2,y2),(0,0,255),2)
            return scale,f
        except:
            return -1,frame
    else:
        return -1,frame

def findCamMTX(pathStr):
    # termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    
    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((4*7,3), np.float32)
    objp[:,:2] = np.mgrid[0:7,0:4].T.reshape(-1,2)
    
    # Arrays to store object points and image points from all the images.
    objpoints = [] # 3d point in real world space
    imgpoints = [] # 2d points in image plane.
    
    images = glob.glob(pathStr)
    
    for fname in images:
        img = cv2.imread(fname)
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    
        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(gray, (7,4),None)
    
        # If found, add object points, image points (after refining them)
        if ret == True:
            objpoints.append(objp)
    
            corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
            imgpoints.append(corners2)
    
            # Draw and display the corners
            img = cv2.drawChessboardCorners(img, (7,4), corners2,ret)
            #cv2.imshow('img',img)
            #cv2.waitKey(500)
    
    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)
    
    return  ret, mtx, dist       


def undistFrame(img, mtx, dist):
    h,  w = img.shape[:2]
    newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),0,(w,h))

    # undistort
    mapx,mapy = cv2.initUndistortRectifyMap(mtx,dist,None,mtx,(w,h),5)
    newimg = cv2.remap(img,mapx,mapy,cv2.INTER_LINEAR)

    # crop the image
    x,y,w,h = roi
    newimg = newimg[y+5:y+h-5, x+5:x+w-5]

    return newimg
