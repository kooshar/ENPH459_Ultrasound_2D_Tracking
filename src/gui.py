import calibration as cal
import copy
from PIL import Image
import diagonal_crop
import cv2
import numpy as np
import wx
import time
import math
import csv
import serial
from collections import namedtuple
from numpy import NaN


def scalePic(MainFrame):
    squareImgLength=100
    img = np.zeros((400,400), np.uint8)+255
    cv2.rectangle(img,(150,150),(150+squareImgLength,150+squareImgLength),(0,0,0),-1)
    cv2.imshow('image',img) 
    
    while(True):
        cv2.waitKey(1)
        btnLabel = MainFrame.lennabtn.GetLabel() 
        if btnLabel=='Quitting':
            return
        if btnLabel=='calculating':
            scale, img = cal.findscale(MainFrame,squareImgLength)
            if scale==-1:
                MainFrame.SetStatusText('error finding image: scale set to defult value 10')
            elif scale>0:
                MainFrame.lengthScale = scale   
                MainFrame.SetStatusText('Scale set to: '+str(scale))
                cv2.imshow('image',img) 
                cv2.waitKeyEx()
            cv2.destroyAllWindows() 
            return
    

def lenna(MainFrame):
    array = [0,0,0]
    im = Image.open('lenna-crop-area.png')
    
    print(im)
    real_x = 200
    real_y = 250
    real_angle = 0
    base = (real_x,real_y)
    height =350 
    width =400
    cropped_im = diagonal_crop.crop(im, base, real_angle, height, width)
    
    while(True):
        btnLabel = MainFrame.btn.GetLabel() 
        if btnLabel=='Quitting':
            return
        
        cropped_im = diagonal_crop.crop(im, (real_x - height/2 * math.sin(real_angle) - width/2*math.cos(real_angle), real_y - height/2*math.cos(real_angle) + width/2*math.sin(real_angle)), real_angle, height, width)
        opencvImage = cv2.cvtColor(np.array(cropped_im), cv2.COLOR_RGB2BGR)
        #opencvImage = np.array(cropped_im) 
        cv2.imshow('lennaimage',opencvImage)
        
        key = cv2.waitKey(2) & 0xFF
        if key == ord('r') :
            real_angle = real_angle +  3.14 /180.0
        if key== ord('c') :
            real_angle = real_angle - 3.14 /180.0
        if key == ord('j') :
            real_y = real_y + 3
        if key == ord('k') :    
            real_y = real_y - 3
        if key == ord('h') :
            real_x = real_x - 3
        if key == ord('l') :
            real_x = real_x + 3  
             
        array[0] = (real_x - 200) * -1
        array[1] = (real_y - 250) * 1
        array[2] = real_angle
        MainFrame.lennaLock.acquire()
        MainFrame.lennaArray = array
        MainFrame.lennaLock.release()

            
    
def Track(MainFrame):
    # x,y,theta     
    positionsknnRigid = [[.0,.0,.0]]
    currposknnRigid = [.0,.0,.0]
    positionsbfRigid = [[.0,.0,.0]]
    currposbfRigid = [.0,.0,.0]
    #actual lenna position
    act_pos = [[.0,.0,.0]]
    
    minknn = MainFrame.minknn
    minbff = MainFrame.minBff
    LENGTH_SCALE = MainFrame.lengthScale
    mtx = MainFrame.mtx
    dist = MainFrame.dist
    
    frame_count = 0
    playerCount=0
    
    
    orb = cv2.ORB_create()
    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=False)
    bff = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
          
          
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
   
    frameStruct = namedtuple('frameStruct', ['pic','kp','des', 'posknn', 'posbf','time' ,'acc'])
    emptyFrame = frameStruct(NaN,NaN,NaN,NaN,NaN,NaN,0)
    frameArray = []
    for a in range(MainFrame.frame_num):
        frameArray.append(emptyFrame) 
      
      
    MainFrame.webcam = cv2.VideoCapture(MainFrame.camNum)        
    t1 = time.time()
    acc=1
    ret, frame = MainFrame.webcam.read()
    if (ret):            
        frame = cal.undistFrame(frame, mtx, dist)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = clahe.apply(gray)
        kp, des = orb.detectAndCompute(gray,None)
        h,w=gray.shape
        
        while(ret):
            btnLabel = MainFrame.btn.GetLabel() 
            if btnLabel!='Done':
                MainFrame.webcam.release()
                break    
            
            #frame counter
            frame_count = frame_count + 1
            if acc>MainFrame.posAccuracy:#don't save current frame as old frame
                newFrame = frameStruct(frame,kp,des, currposknnRigid,currposbfRigid, t1, acc)
                frameArray.append(newFrame)
                normalizeAccuracies(frameArray)
                frameArray.pop(0)
            
            
            print('frametime:',currposknnRigid, str(time.time()-t1))
            
            #get a new frame, change to gray scale, and extract descriptions      
            t1 = time.time()
            ret, frame = MainFrame.webcam.read()
            frame = cal.undistFrame(frame, mtx, dist) 
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            gray = clahe.apply(gray)
            kp, des = orb.detectAndCompute(gray,None)
            newFrame = frameStruct(frame,kp,des, [NaN,NaN,NaN],[NaN,NaN,NaN], t1, 0)
            
            print('accuracies')
            for a in range(MainFrame.frame_num):
                print(a,frameArray[a].acc)
            
            currposknnEstArr=[]
            currposbfEstArr=[]
            currposAccArr=[]
            for a in range(MainFrame.frame_num):
                if not math.isnan(frameArray[a].time): 
                    oldkp = frameArray[a].kp
                    olddes = frameArray[a].des
                    oldAcc = frameArray[a].acc
                    oldcurrposknnRigid = frameArray[a].posknn
                    oldcurrposbfRigid = frameArray[a].posbf
                    
                    #match the descriptions
                    if (olddes is not None and len(kp)>2):
                        if(des is not None and len(oldkp)>2):
                            p, oldp, pbf, oldpbf = findPairPoints(bf, bff, kp, des, oldkp, olddes)    
                             
                            #find the frame distance  
                            if len(p)>minknn and len(pbf)>minbff:
                                changeknnRigid, changebfRigid, accuracy = findTransforms(p, oldp, pbf, oldpbf,LENGTH_SCALE,oldcurrposknnRigid[2],oldcurrposbfRigid[2])
                                
                                
                                currposknnEstArr.append([x + y for x, y in zip(oldcurrposknnRigid, changeknnRigid)])
                                currposbfEstArr.append([x + y for x, y in zip(oldcurrposbfRigid, changebfRigid)])
                                currposAccArr.append(accuracy*oldAcc)
            
            
            if len(currposAccArr)>0:
                currposknnRigid,currposbfRigid,acc = estimatePositionAndAccuracy(currposknnEstArr,currposbfEstArr,currposAccArr)     
                            
                positionsbfRigid.append(currposbfRigid)
                positionsknnRigid.append(currposknnRigid)
                    
                #IMU Data
                #line = MainFrame.sio.readline()
                #str = line[line.find("="):]
                #print(str)
                
                MainFrame.lennaLock.acquire()
                copy_array = copy.deepcopy(MainFrame.lennaArray)
                MainFrame.lennaLock.release()
                act_pos.append(copy_array)
           
                MainFrame.SetStatusText('Frame:'+str(frame_count)+' Pos: '+str(currposknnRigid)+' matches:' +' map updated successfully!')
                       
            #show the data
            playerCount=playerCount+1
            if playerCount>2 :
                trackPad = getTrackPadImg(MainFrame, positionsbfRigid, positionsknnRigid, currposbfRigid, currposknnRigid)
                for m in kp:  
                    cv2.circle(gray,(int(m.pt[0]),int(m.pt[1])), 2, (0,0,255), -1)
                
                updatePlayer(MainFrame, gray, trackPad,w,h)
                playerCount=0
            
        if(ret):        
            MainFrame.SetStatusText("Done!")
        else:
            MainFrame.SetStatusText("camera got disconnected!")
        
        ptbf=np.array(positionsbfRigid)
        ptknn=np.array(positionsknnRigid)
        ptact=np.array(act_pos)
        
        np.savetxt("data" + str(time.time()) + ".csv",np.hstack((ptbf,ptknn,ptact)),delimiter=',')
    else:
        MainFrame.SetStatusText("Camera not detected. Tracking did not start.") 
    
    MainFrame.webcam.release()
    
    
def Initialize(MainFrame):
    mtx=MainFrame.mtx
    dist=MainFrame.dist
    
    MainFrame.webcam = cv2.VideoCapture(MainFrame.camNum)#to load a file change to:cv2.VideoCapture('file.avi')
    orb = cv2.ORB_create()
    
    #get the first two frames
    if(MainFrame.webcam.isOpened()):                  
        ret, frame = MainFrame.webcam.read()
        f = cal.undistFrame(frame, mtx, dist)
        gray = cv2.cvtColor(f, cv2.COLOR_BGR2GRAY)
        h,w=gray.shape
    
        i=0    
        while(ret):
            btnLabel = MainFrame.btn.GetLabel()
            if btnLabel!='Start Tracking':
                MainFrame.webcam.release()
                return
            
            frame = cal.undistFrame(frame, mtx, dist)
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            kp1,_  = orb.detectAndCompute(gray,None)
            if len(kp1)!=0 :
                for m in kp1:
                    cv2.circle(gray,(int(m.pt[0]),int(m.pt[1])), 2, (0,0,255), -1)
            i=i+1
            if (i==2):
                i=0
                rgb=cv2.cvtColor(gray,cv2.COLOR_GRAY2RGB)
                imageScale=min((MainFrame.btn.GetPosition()[1]-20)*1.0/h,(MainFrame.ClientSize[0]-MainFrame.trackPlayer.Size[0]-10)*1.0/w)
                rgb=cv2.resize(rgb, (int(w*imageScale),int(h*imageScale))) 
                image = wx.Bitmap.FromBuffer(int(w*imageScale),int(h*imageScale), rgb)
                MainFrame.vidPlayer.SetBitmap(image)
                                
            ret, frame = MainFrame.webcam.read()
    else:
        print('camera not started')
        

    
def findPairPoints(bf, bff, kp, des, oldkp, olddes):  
    matchesknn = bf.knnMatch(des,olddes,k=2)
    matchesbf = bff.knnMatch(des,olddes,k=1)
    
    # store all the good matches as per Lowe's ratio test.
    p = []
    oldp = []
    for m,n in matchesknn:
        if m.distance < 0.9*n.distance:
            p.append(kp[m.queryIdx].pt)
            oldp.append(oldkp[m.trainIdx].pt)
    p = np.float32(p)
    oldp = np.float32(oldp)
    
    pbf = []
    oldpbf = []
    for m in matchesbf:
        if len(m)>0:
            pbf.append(kp[m[0].queryIdx].pt)
            oldpbf.append(oldkp[m[0].trainIdx].pt)
    pbf = np.float32(pbf)
    oldpbf = np.float32(oldpbf)
        
    print('test:',len(p),len(oldp),len(pbf),len(oldpbf))
    return p, oldp, pbf, oldpbf

def findTransforms(p, oldp, pbf, oldpbf, LENGTH_SCALE, yawknn, yawbf):
    
    rigid = cv2.estimateRigidTransform(p, oldp, fullAffine=False)
    if rigid is not None:
        Yaw_dist_knn = yawknn-math.atan(rigid[0,1]/rigid[0,0])
        changeknnRigid = [(rigid[0,2]*math.cos(Yaw_dist_knn)-rigid[1,2]*math.sin(Yaw_dist_knn))/LENGTH_SCALE,(rigid[0,2]*math.sin(-Yaw_dist_knn)+rigid[1,2]*math.cos(-Yaw_dist_knn))/LENGTH_SCALE,Yaw_dist_knn - yawknn]
    
        # use bf+the rigid transformation between frames
        rigid = cv2.estimateRigidTransform(pbf, oldpbf, fullAffine=False)
        if rigid is not None:
            Yaw_dist_bf = yawbf-math.atan(rigid[0,1]/rigid[0,0])
            changebfRigid = [(rigid[0,2]*math.cos(Yaw_dist_bf)-rigid[1,2]*math.sin(Yaw_dist_bf))/LENGTH_SCALE,(rigid[0,2]*math.sin(-Yaw_dist_bf)+rigid[1,2]*math.cos(-Yaw_dist_bf))/LENGTH_SCALE,Yaw_dist_bf - yawbf]
        
            #check the difference of the two method
            xacc = abs((changeknnRigid[0]-changebfRigid[0]))/(abs(changeknnRigid[0])+abs(changebfRigid[0]))
            yacc = abs((changeknnRigid[1]-changebfRigid[1]))/(abs(changeknnRigid[1])+abs(changebfRigid[1]))
            accuracy = (xacc+yacc)/2.0
            accuracy = 1-accuracy
            
            if accuracy<0:
                print('acc less than 0')
                print(len(p),len(oldp),len(pbf),len(oldpbf))
                accuracy=0
            
        else:
            changeknnRigid = [.0,.0,.0]
            changebfRigid = [.0,.0,.0]
            print('acc none bf')
            print(len(p),len(oldp),len(pbf),len(oldpbf))
            accuracy = 0
    else:
        changeknnRigid = [.0,.0,.0]
        changebfRigid = [.0,.0,.0]
        print('acc none knn')
        print(len(p),len(oldp),len(pbf),len(oldpbf))
        accuracy=0
    return changeknnRigid, changebfRigid, accuracy 

def getTrackPadImg(MainFrame, positionsbfRigid, positionsknnRigid, currposbfRigid, currposknnRigid):
    pts2 = np.array(positionsbfRigid, np.int32)[0:,0:2]+[MainFrame.trackPlayer.Size[1]/2,MainFrame.trackPlayer.Size[0]/2]
    pts3 = np.array(positionsknnRigid, np.int32)[0:,0:2]+[MainFrame.trackPlayer.Size[1]/2,MainFrame.trackPlayer.Size[0]/2]
    size = MainFrame.trackPlayer.DoGetSize()
    trackPad = np.zeros([size[0],size[1],3], np.uint8)+125
    cv2.polylines(trackPad,[pts2],False,(0,255,0))#green
    cv2.polylines(trackPad,[pts3],False,(255,0,155))#pink
    
    tail=(MainFrame.trackPlayer.Size[1]/10,MainFrame.trackPlayer.Size[0]/10)
    tip = (tail[0]+int(MainFrame.trackPlayer.Size[1]/10*math.sin(currposbfRigid[2])),tail[1]+int(MainFrame.trackPlayer.Size[0]/10.*math.cos(currposbfRigid[2])))
    cv2.arrowedLine(trackPad, tail, tip, (0,0,0), 1)
    tail=(MainFrame.trackPlayer.Size[1]/10,MainFrame.trackPlayer.Size[0]/10)
    tip = (tail[0]+int(MainFrame.trackPlayer.Size[1]/10*math.sin(currposknnRigid[2])),tail[1]+int(MainFrame.trackPlayer.Size[0]/10.*math.cos(currposknnRigid[2])))
    cv2.arrowedLine(trackPad, tail, tip, (255,255,255), 1)
    return trackPad

def updatePlayer(MainFrame, gray, trackPad, w, h):
    rgb=cv2.resize(trackPad, MainFrame.trackPlayer.DoGetSize()) 
    image = wx.Bitmap.FromBuffer(MainFrame.trackPlayer.Size[0],MainFrame.trackPlayer.Size[1], rgb)
    MainFrame.trackPlayer.SetBitmap(image)
    rgb=cv2.cvtColor(gray,cv2.COLOR_GRAY2RGB)
    imageScale=min((MainFrame.btn.GetPosition()[1]-20)*1.0/h,(MainFrame.ClientSize[0]-MainFrame.trackPlayer.Size[0]-10)*1.0/w)
    rgb=cv2.resize(rgb, (int(w*imageScale),int(h*imageScale))) 
    image = wx.Bitmap.FromBuffer(int(w*imageScale),int(h*imageScale), rgb)
    MainFrame.vidPlayer.SetBitmap(image)
    
    
def normalizeAccuracies(frameArray):
    maxAcc = max([element.acc for element in frameArray])
    for a in range(len(frameArray)):
        frameArray[a]=frameArray[a]._replace(acc=frameArray[a].acc/maxAcc)
         
def estimatePositionAndAccuracy(currposknnEstArr,currposbfEstArr,currposAccArr):
    #METHOD 1: Averaging
    #currposknnRigid = [sum(element[0] for element in currposknnEstArr)/(a), sum(element[1] for element in currposknnEstArr)/(a),sum(element[2] for element in currposknnEstArr)/(a)]
    #currposbfRigid =[sum(element[0] for element in currposbfEstArr)/(a), sum(element[1] for element in currposbfEstArr)/(a),sum(element[2] for element in currposbfEstArr)/(a)]
    #acc =math.fsum(currposAccArr)/(a)

    #METHOD 2: find the best acc
    accIndex = currposAccArr[0]
    imgIndex = 0
    for a in range(len(currposAccArr)):
        if currposAccArr[a]>accIndex:
            imgIndex = a
            accIndex = currposAccArr[a]
     
    currposknnRigid = currposknnEstArr[imgIndex]
    currposbfRigid = currposbfEstArr[imgIndex]
    acc = accIndex
    
    return currposknnRigid,currposbfRigid,acc
    
       