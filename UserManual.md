# User Manual

## Introduction
This document contains the user manual for the Ultrasound Tracker program. The
program was designed as a part of the project taken by our student engineering 
team from Sept 2017 to May 2018 as part of the ENPH 459 course at the University
of British Columbia which is a student project course for 4th year Engineering 
Physics students in the faculty of Applied Science.

This project was sponsored by Dr. Antony Hodgson, head of the Hodgsonlab in the 
Universty of British Columbia's mechanical engineering department.

The Authors of the project are Aashish Karna, Koosha Rezaiezadeh, and Jim Shaw.

## Step 1: Connect the camera

Connect the desired camera to the computer through a USB port. 

## Step 2: Loading Camera Calibration Images
The calibration images are a set of images of a 5x8 chessboard pattern taken 
from different angles as shown in the image below. These images help the 
algorithm calibrate itself. This should be done **__before__** running the 
program, to prevent any errors.
![](docs/calibration.PNG)

## Step 2: Starting the program

Run the python file **gui_new.py**. Make sure you have all the necessary 
libraries installed beforehand to avoid any errors.

Once correctly started, the following **start screen** will be shown:

![](docs/startpage.PNG)

## Step 3: Calibration

On the start page, click the **Calibrate** button. This will calibrate the
camera using the files that were loaded in *Step 2*. Once successful, the 
following screen will be displayed, with the message below reading 
"**Calibrated! Ready to Start!**"

![](docs/calibrate.PNG)

## Step 4: Select the camera

From the drop down on the screen, select the camera to be used for tracking. 
If there is only **one camera** connected, select the default, i.e **Camera #0**
If there are more than one cameras connected, select the one which is to be used

## Step 5: Initialize

Once the right camera is selected, click the **Initialize** button. This will
open up the camera and display the live feed on the window as shown in the image
below. (The black dots on the live feed show the features being detected)

![](docs/initialize.PNG)

## Step 6: Load Scale

To ensure that the units of measurement are consistent, the algorithm must be
scaled correctly. To do this, click on the **Load Scale** button. This will 
create a new window with a black square - **Ignore this window**. 

![](docs/loadscale.PNG)



### Place a patterned image below the camera, this should appear on the live feed as shown below

![](docs/alignscale.PNG)

## Step 7: Calculate the scaling factor

Once the pattern is placed below the camera and is in full focus, click on the
**Calc. Scale** button to calculate the scaling factor needed to output the 
measurements in the correct dimensions. When the button is clicked, a new window
will pop up, with a shot of the pattern outlined in red and the scale factor 
value at the bottom of the window. Press **Q** to exit this window once you are
satisfied with the scaling factor value. The scaling module can be run several
times until a reasonable value is achieved

![](docs/calculatescale.PNG)


### Another window will pop up, with the generic image of a lady (Lenna) as shown below. This can be ignored as it is a feature for testing purposes.

![](docs/lenna.PNG)

## Step 8: Start Tracking

Once the scaling is complete, the system is now ready to begin tracking. The 
window should look like the image below. (If this is not the state of your 
system, trace a few steps back to see where the issue might be located.) Click 
on the **Start Tracking** button to begin the tracking process

![](docs/beforetracking.PNG)


### The window should look similar to the image below when the tracking algorithm is running. The gray box on the right shows the path traced out (pink lines) and the angle of orientation (white arrow)

![](docs/tracking.PNG)

## Step 9: Ending the process and saving the data

Clicking the **Done** button will end the tracking process and will store the 
data points in a time-stamped csv file in the source folder. The window should 
return back to the **start screen** once the process is completed.

![](docs/done.PNG)
